const login = require('facebook-chat-api');
const fs = require('fs');
const path = require('path');
const fileSys = require('./UTILS/FileManager/FileManager.js');
const $ = require('lodash');

const FistInit = {
    score: 0,
    level: 0
};

login({appState: JSON.parse(fs.readFileSync('appstate.json', 'utf8'))}, (err, api) => {
    if(err) return console.error(err);
            api.setOptions({
                selfListen: false,
                logLevel: "silent"
            });

    api.listen((err, message) => {
        if(err) return console.error(err);

        switch(message.body){
            case 'ping':
                api.sendMessage('pong', message.threadID);
                break;
            case 'blacklist':
                let blacklistlib = JSON.parse(fs.readFileSync('./kick/badword.json'));
                api.sendMessage(JSON.stringify(blacklistlib), message.threadID);
                break;
            case "test":
                api.sendMessage({sticker:387545608037990},message.threadID);
            default:
                break;
        }

        function viewLog() {
            console.log('-------------------------\nmsg\n' + message.body + '\n-------------------------\nUID\n' + message.senderID + '\n-------------------------');
        }
        /*SEND STICKER*/
        if(message.type == "message") {
            let emoji = JSON.parse(fs.readFileSync('./DATA/FLATDB/EMOJILIST.json'));
            emoji.forEach(element => {
                if(element == message.body) {
                    fs.readdir(path.join(__dirname, 'DATA', 'sticker'), (err, files) => {
                        let ran = Math.floor(Math.random() * parseInt(files.length));
                        return Promise
                            .resolve({
                                attachment : fs.createReadStream(path.join(__dirname, 'data', 'sticker', files[ran]))
                            })
                            .then(a => api.sendMessage(a, message.threadID));
                    });
                }
            });
        }
        /*ADD NEW EMOJI TO SYSTEM*/
        if(message.body.indexOf('sticker -> ') == 0) {
            let emoji = JSON.parse(fs.readFileSync('./DATA/FLATDB/EMOJILIST.json'));
            let wordArr = fileSys.toArray(emoji);
            if(fileSys.exist(emoji, wordArr))
            {
                // Avoid
            }
            else
            {
                fileSys.saveArrayToFile(fileSys.addToArrayIfNeeded(message.body.slice(
                    11, message.body.length), wordArr), './DATA/FLATDB/EMOJILIST.json');
            }
        }
        /*ADD TO BLACKLIST*/
        if(message.body.indexOf('!! ') == 0) {
            let badWords = message.body.slice(3, message.body.length);
            let oldBadWordList = JSON.parse(fs.readFileSync('./DATA/FLATDB/BADWORD.json'));
            let wordArr = fileSys.toArray(oldBadWordList);
            if(wordArr.indexOf(badWords) != -1){
                api.sendMessage("Đã có trong hệ thống..", message.threadID);
            } else {
                fileSys.saveArrayToFile(fileSys.addToArrayIfNeeded(badWords, wordArr), './DATA/FLATDB/BADWORD.json');
            }
        }
        /*CHECK BADWORD*/
        if(message.body.indexOf('del ') == -0 || message.body.indexOf('!!') == 0) {
            // nothing
        } else {
            let badWords = JSON.parse(fs.readFileSync('./DATA/FLATDB/BADWORD.json', 'utf-8'));
            badWords.forEach(function(element) {
                if(message.body.indexOf(element) > -1) {
                        api.removeUserFromGroup(message.senderID, message.threadID, (err));
                }
            });
        }
        /*MAKE VOICE LIKE GG TRANSLATE :3
        ENJOY :3*/
        if(message.body.indexOf("say ") != -1) {
            const say = require("./UTILS/Say/say.js");
            say(message.body.slice(4, message.body.length), function() {
                let m = { body : "", attachment : fs.createReadStream(__dirname + '/UTILS/Say/src/say.mp3') };
                 api.sendMessage(m, message.threadID);
            });
        }
        /*Proc message*/
        if(message.type == 'message') {
            if(fs.existsSync('./DATA/USR/INFO/' + message.senderID + '.json')) {
                let data = JSON.parse(fs.readFileSync('./DATA/USR/INFO/' + message.senderID + '.json'));
                let score = parseInt(data.score);
                score += 1;
                let level = Math.floor(score / 150);
                let newData = {
                    score: score,
                    level: level,
                }
                fs.writeFileSync(
                    './DATA/USR/INFO/' + message.senderID + '.json', JSON.stringify(newData), 'utf-8');
            } else {
                fileSys.createNewFile('./DATA/USR/INFO/' + message.senderID + '.json');
            fs.writeFileSync(
                './DATA/USR/INFO/' + message.senderID + '.json', JSON.stringify(FistInit), 'utf-8');
            }
        }
        /*CHECK RANK*/
        if(message.body === ".rank") {
            if(fs.existsSync('./DATA/USR/INFO/' + message.senderID + '.json')) {
                let usrObj = JSON.parse(fs.readFileSync('./DATA/USR/INFO/' + message.senderID + '.json'));
                api.sendMessage('Score: ' + usrObj.score + '\nLevel: ' + usrObj.level, message.threadID);
            } else {
                fileSys.createNewFile('./DATA/USR/INFO/' + message.senderID + '.json');
                fs.writeFileSync(
                    './DATA/USR/INFO/' + message.senderID + '.json', JSON.stringify(FistInit), 'utf-8');
            }
        }
        /*SETUP WORD TO AUTO-REPLY*/
        if(message.body.indexOf('->') != -1 && message.body.indexOf("sticker ") == -1) {
            let Arr1 = JSON.parse(fs.readFileSync('./DATA/FLATDB/MESSAGETOMESSAGE.json'));
            let Arr2 = message.body.split('->', 2);
            let newObj = { message : Arr2[0], reply : Arr2[1] };
            Arr1.push(newObj);
            fs.writeFileSync('./DATA/FLATDB/MESSAGETOMESSAGE.json', JSON.stringify(Arr1), 'utf-8');
        }
        /*CHECK*/
        if(message.type == 'message') {
            let MTM = JSON.parse(fs.readFileSync('./DATA/FLATDB/MESSAGETOMESSAGE.json'));
            MTM.forEach(element => {
                if(message.body == element.message) {
                    api.sendMessage(element.reply, message.threadID);
                }
            });
            let MTS = JSON.parse(fs.readFileSync("./DATA/FLATDB/MESSAGETOSTICKER.json"));
            MTS.forEach(element => {
                if(message.body == element.message) {
                    api.sendMessage({ sticker : element.sticker }, message.threadID);
                }
            });
        }
        /*delete something*/
        if(message.body.indexOf('del ') == 0) {
            let wordIn = message.body.slice(4, message.body.length);
            let oldBadwordList = JSON.parse(fs.readFileSync('./DATA/FLATDB/BADWORD.json'));
            let wordArr = fileSys.toArray(oldBadwordList);

            let outputArray = wordArr.filter(function(element) {
                return element !== wordIn
            });
            fileSys.saveArrayToFile(outputArray, './DATA/FLATDB/BADWORD.json');
            //--
            let oldMTMList = JSON.parse(fs.readFileSync('./DATA/FLATDB/MESSAGETOMESSAGE.json'));
            let outputMTMArray = oldMTMList.filter(function(e) {
                return e.message !== wordIn
            });
            fs.writeFileSync('./DATA/FLATDB/MESSAGETOMESSAGE.json', JSON.stringify(outputMTMArray), "utf-8");
            //--
            let oldMTSList = JSON.parse(fs.readFileSync('./DATA/FLATDB/MESSAGETOSTICKER.json'));
            let outputMTSArray = oldMTSList.filter(function(e) {
                return e.message !== wordIn
            });
            fs.writeFileSync('./DATA/FLATDB/MESSAGETOSTICKER.json', JSON.stringify(outputMTSArray), "utf-8");
        }
        /*Sticker*/
        if(message.body.indexOf("def ") != -1) {
            if(fs.existsSync("./DATA/USR/EXC/" + message.senderID + ".txt")) {
                fs.writeFileSync("./DATA/USR/EXC/" + message.senderID + ".txt", message.body, "utf-8");
            } else {
                fileSys.createNewFile("./DATA/USR/EXC/" + message.senderID + ".txt");
                fs.writeFileSync("./DATA/USR/EXC/" + message.senderID + ".txt", message.body, "utf-8");
            }
        }
        if(message.type == "message"){
            if(message.attachments != ""){
                if(fs.existsSync("./DATA/USR/EXC/" + message.senderID + ".txt")) {
                    let dataEx = fs.readFileSync("./DATA/USR/EXC/" + message.senderID + ".txt");
                    if(message.attachments[0].type == "sticker" && dataEx != "" && dataEx.indexOf("def ") != -1) {
                        let List = JSON.parse(fs.readFileSync('./DATA/FLATDB/MESSAGETOSTICKER.json'));
                        let msg = dataEx.slice(4, dataEx.length);
                        let msgString = msg.toString();
                        let stickerID = message.attachments[0].ID;
                        let obj = {
                            message : msgString,
                            sticker : stickerID,
                        };
                        List.push(obj);
                        fs.writeFileSync('./DATA/FLATDB/MESSAGETOSTICKER.json',JSON.stringify(List),"utf-8");
                        fs.writeFileSync("./DATA/USR/EXC/" + message.senderID + ".txt","");
                    }
                }
            }
        }

        if(message.body.indexOf("def ") != -1) {
            api.sendMessage("Gửi một nhãn dán", message.threadID);
        }
        if(message.senderID == '100030422649974') {
            if(message.body.indexOf("lineStore ") == 0) {
                let line = require("./UTILS/LineDownloader/LineDownloader.js");
                let url = message.body.slice(10, message.body.length);
                if(url != "")
                    line.get(url);
            }
        }
        viewLog();
    });
});
