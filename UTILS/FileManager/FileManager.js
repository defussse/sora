const fs = require('fs');

module.exports = {
    createNewFile: (filename) => {
        const fd = fs.openSync(filename, 'w');
    },
    saveArrayToFile: (arr, file) => {
        fs.writeFileSync(file, JSON.stringify(arr), 'utf-8');
    },
    addToArrayIfNeeded: (element, arr)  => {
        let newArr = arr;
        newArr.push(element);
        return newArr;
    },
    toArray: (obj) => {
        let wordArr = [];
        wordArr = wordArr.concat(obj);
        return wordArr;
    },
    exist: (word, array) => {
        return array.indexOf(word) >= 0;
    }
}
